#!/bin/bash

set -eux

if [[ ! -d /app/data/synapse ]]; then
    echo "=> Detected first run"

    # create dirs (for whatever reason the nginx dir couldn't get created by  nginx)
    mkdir -p /app/data/synapse \
             /app/data/nginx/fastcgi \
             /run/nginx_log \
             /run/synapse \
             /run/turn_log

    # fix permissions
    chown -R www-data.www-data /run/synapse \
                               /run/turn_log \
                               /app/data

    # copy turn config
    cp /usr/share/coturn/examples/etc/turnserver.conf /app/data/turnserver.conf

    # copy email templates for matrix
    cp -r /app/code/synapse/synapse/res/templates /app/data/templates

    # set default TURN listening port
    if [ -z ${TURN_TLS_PORT+x} ]; then TURN_TLS_PORT="5349"; else echo "TURN_TLS_PORT is set to '$TURN_TLS_PORT'"; fi

    mkdir -p /app/data/synapse
    cd /app/data/synapse
    python -m synapse.app.homeserver \
	    --server-name ${APP_DOMAIN} \
	    --config-path homeserver.yaml \
	    --report-stats=no \
	    --generate-config

    # create certs
    openssl req -new -newkey rsa:4096 -x509 -sha256 -days 365 -nodes -out ${APP_DOMAIN}.tls.crt -keyout ${APP_DOMAIN}.tls.key -subj "/C=DE/ST=Germany/L=Germany/O=Example/OU=Example/CN=${APP_DOMAIN}"
    
    # synapse config
    sed -i "s/server_name:.*/server_name: ${APP_DOMAIN}/" homeserver.yaml
    sed -i "s/web_client:.*/web_client: False/" homeserver.yaml
    sed -i "s,- webclient .*,# - webclient  # The bundled webclient," homeserver.yaml
    sed -i "s/client, webclient/client/" homeserver.yaml
    sed -i "s/sqlite3/psycopg2/" homeserver.yaml
    sed -i "s/    database: .*/    user: ${POSTGRESQL_USERNAME}\n    password: ${POSTGRESQL_PASSWORD}\n    database: ${POSTGRESQL_DATABASE}\n    host: ${POSTGRESQL_HOST}\n    cp_min: 5\n    cp_max: 10/" homeserver.yaml
    sed -i "s/enable_registration: .*/enable_registration: True/" homeserver.yaml
    sed -i "s/# password_providers:/password_providers:/" homeserver.yaml
    sed -i 's/#     - module: "ldap_auth_provider.LdapAuthProvider"/     - module: "ldap_auth_provider.LdapAuthProvider"/' homeserver.yaml
    sed -i 's/#       config:/       config:/' homeserver.yaml
    sed -i 's/#         enabled: true/         enabled: true/' homeserver.yaml
    sed -i "s,#         uri: .*,         uri: \"${LDAP_URL}\"," homeserver.yaml
    sed -i 's/#         start_tls: true/         start_tls: false/' homeserver.yaml
    sed -i 's/#         base: "ou=users,dc=example,dc=com"/         base: "ou=users,dc=cloudron"/' homeserver.yaml
    sed -i 's/#         attributes:/         attributes:/' homeserver.yaml
    sed -i 's/#            uid: "cn"/           uid: "username"/' homeserver.yaml
    sed -i 's/#            mail: "email"/           mail: "mail"/' homeserver.yaml
    sed -i 's/#            name: "givenName"/           name: "username"/' homeserver.yaml
    sed -i 's/max_upload_size:.*/max_upload_size: "20M"/' homeserver.yaml
    sed -i 's/#auto_join_rooms:/auto_join_rooms:/' homeserver.yaml
    sed -i 's/#    - "#example:example.com"/    - "#example:example.com"/' homeserver.yaml
    sed -i "s/example:example.com/discuss:${APP_DOMAIN}/" homeserver.yaml
    sed -i "s/turn_allow_guests:.*/turn_allow_guests: False/" homeserver.yaml
    sed -i "s/enable_group_creation:.*/enable_group_creation: True/" homeserver.yaml
    sed -i "s/enable_group_creation:.*/enable_group_creation: True/" homeserver.yaml
    sed -i "s/#user_directory:/user_directory:/" homeserver.yaml
    sed -i "s/#   search_all_users:.*/   search_all_users: True/" homeserver.yaml

    # get synapse log name and set path
    LOG_CONFIG=$(ls *.log.config)
    sed -i "s/log_file: .*/log_file: \"\/run\/synapse\/homeserver.log\"/" homeserver.yaml
    sed -i "s/    filename:.*/    filename: \/run\/synapse\/homeserver.log/" ${LOG_CONFIG}


    # coturn
    TURNPWD=$(pwgen -s 64 1)
    sed -i "s/#tls-listening-port=5349/tls-listening-port=5349/" /app/data/turnserver.conf
    sed -i "s/#realm=mycompany.org/realm=${APP_DOMAIN}/" /app/data/turnserver.conf
    sed -i "s/#lt-cred-mech/lt-cred-mech/" /app/data/turnserver.conf
    sed -i "s/#use-auth-secret/use-auth-secret/" /app/data/turnserver.conf
    sed -i "s/#lt-cred-mech/lt-cred-mech/" /app/data/turnserver.conf
    sed -i "s/#static-auth-secret=.*/static-auth-secret=${TURNPWD}/" /app/data/turnserver.conf
    sed -i "s/turn_uris: .*/turn_uris: [\"turn:${APP_DOMAIN}:${TURN_TLS_PORT}?transport=udp\", \"turn:${APP_DOMAIN}:${TURN_TLS_PORT}?transport=tcp\"]/" homeserver.yaml
    sed -i "s/turn_shared_secret: .*/turn_shared_secret: \"${TURNPWD}\"/" homeserver.yaml
    sed -i "s/#cipher-list=.*/cipher-list=\"HIGH\"/" /app/data/turnserver.conf
    sed -i "s/#log-file=.*/log-file=\/run\/turn_log\/turn.log/" /app/data/turnserver.conf


    # get cert names from synapse
    TLS_CRT=$(ls *.tls.crt)
    TLS_KEY=$(ls *.tls.key)
    sed -i "s/#cert=.*/cert=\/app\/data\/synapse\/${TLS_CRT}/" /app/data/turnserver.conf
    sed -i "s/#pkey=.*/pkey=\/app\/data\/synapse\/${TLS_KEY}/" /app/data/turnserver.conf

    # set up email
    sed -i 's/#email:/email:/' homeserver.yaml
    sed -i 's/#   enable_notifs: false/   enable_notifs: false/' homeserver.yaml
    sed -i 's/#   smtp_host: \"localhost\"/   smtp_host: \"${MAIL_SMTP_SERVER}\"/' homeserver.yaml
    sed -i 's/#   smtp_port: .*/   smtp_port: ${MAIL_SMTP_PORT}/' homeserver.yaml
    sed -i 's/#   smtp_user: \"exampleusername\"/   smtp_user: \"${MAIL_SMTP_USERNAME}\"/' homeserver.yaml
    sed -i 's/#   smtp_pass: \"examplepassword\"/   smtp_pass: \"${MAIL_SMTP_PASSWORD}\"/' homeserver.yaml
    sed -i 's/#   require_transport_security: False/   require_transport_security: True/' homeserver.yaml
    sed -i "s/#   notif_from: .*/   notif_from: \"Your Friendly Matrix Home Server <${MAIL_FROM}>\"/" homeserver.yaml
    sed -i 's/#   app_name: Matrix/   app_name: Matrix/' homeserver.yaml
    sed -i 's/#   template_dir: res\/templates/   template_dir: \/app\/data\/templates/' homeserver.yaml
    sed -i 's/#   notif_template_html: notif_mail.html/   notif_template_html: notif_mail.html/' homeserver.yaml
    sed -i 's/#   notif_template_text: notif_mail.txt/   notif_template_text: notif_mail.txt/' homeserver.yaml
    sed -i 's/#   notif_for_new_users: True/   notif_for_new_users: True/' homeserver.yaml

fi

    mkdir -p /app/data/synapse \
             /app/data/nginx/fastcgi \
             /run/nginx_log \
             /run/synapse \
             /run/turn_log

    # fix permissions
    chown -R www-data.www-data /run/synapse \
                               /run/turn_log \
                               /app/data



chown -R www-data.www-data /app/data

cd /app/data/synapse

# check if TURN port has changed and update it
if [ -z ${TURN_TLS_PORT+x} ]; then TURN_TLS_PORT="5349"; fi
sed -i "s/turn_uris: .*/turn_uris: [\"turn:${APP_DOMAIN}:${TURN_TLS_PORT}?transport=udp\", \"turn:${APP_DOMAIN}:${TURN_TLS_PORT}?transport=tcp\"]/" homeserver.yaml
sed -i "s/tls-listening-port=.*/tls-listening-port=${TURN_TLS_PORT}/" /app/data/turnserver.conf

# check if certificate changed and update fingerprint
TLS_FINGERPRINT=$(openssl s_client -connect ${APP_DOMAIN}:${TURN_TLS_PORT} < /dev/null 2> /dev/null | openssl x509 -outform DER | openssl sha256 -binary | base64 | tr -d '=')
sed -i "s,^tls_fingerprints:.*,tls_fingerprints: [{sha256: \"${TLS_FINGERPRINT}\"}]," homeserver.yaml

gosu www-data turnserver -c /app/data/turnserver.conf --daemon -v

# update user and pass in case they changed
sed -i "s/ user: .*/ user: ${POSTGRESQL_USERNAME}/" homeserver.yaml
sed -i "s/ password: .*/ password: ${POSTGRESQL_PASSWORD}/" homeserver.yaml
sed -i "s/ database: .*/ database: ${POSTGRESQL_DATABASE}/" homeserver.yaml
sed -i "s,         uri: .*,         uri: \"${LDAP_URL}\"," homeserver.yaml

#update email settings in case they changed
sed -i "s/   smtp_host:.*/   smtp_host: \"${MAIL_SMTP_SERVER}\"/" homeserver.yaml
sed -i "s/   smtp_port:.*/   smtp_port: ${MAIL_SMTP_PORT}/" homeserver.yaml
sed -i "s/   smtp_user:.*/   smtp_user: \"${MAIL_SMTP_USERNAME}\"/" homeserver.yaml
sed -i "s/   smtp_pass:.*/   smtp_pass: \"${MAIL_SMTP_PASSWORD}\"/" homeserver.yaml

gosu www-data python -m synapse.app.homeserver --config-path homeserver.yaml &> /dev/null &

exec /usr/sbin/nginx -g 'daemon off;'