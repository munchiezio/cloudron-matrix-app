FROM cloudron/base:1.0.0@sha256:147a648a068a2e746644746bbfb42eb7a50d682437cead3c67c933c546357617

MAINTAINER Authors name <support@cloudron.io>

RUN mkdir -p /app/code/nginx

WORKDIR /app/code

EXPOSE 8000

# nginx
RUN rm /etc/nginx/sites-enabled/*
ADD nginx_matrix.conf /etc/nginx/sites-enabled/
RUN rm -rf /var/lib/nginx && ln -sf /app/data/nginx /var/lib/nginx
RUN rm -rf /var/log/nginx && ln -sf /run/nginx_log /var/log/nginx

# TURN
RUN apt update && apt-get install -y coturn
RUN sed -e 's,#TURNSERVER_ENABLED=1,TURNSERVER_ENABLED=1,' -i /etc/default/coturn \
        && rm /etc/turnserver.conf \
	&& ln -s /app/data/turnserver.conf /etc/turnserver.conf

# Synapse
# a local copy is needed because of the mail templates, else the pip installation would suffice
RUN git clone https://github.com/matrix-org/synapse.git
RUN cd /app/code/synapse && git pull origin master

RUN apt-get install -y build-essential python2.7-dev libffi-dev libldap2-dev libsasl2-dev \
	    python-pip python-setuptools sqlite3 \
	    libssl-dev libjpeg-dev libxslt1-dev
RUN pip install --upgrade pip
RUN pip install --upgrade setuptools
RUN pip install psycopg2-binary py-bcrypt matrix-synapse-ldap3 lxml netaddr twisted
RUN pip install https://github.com/matrix-org/synapse/archive/v0.99.3.2.tar.gz

# copy index.html
COPY index.html /app/code/nginx

RUN chown -R www-data.www-data /app/code

ADD start_matrix.sh /app/

CMD [ "/app/start_matrix.sh" ]
